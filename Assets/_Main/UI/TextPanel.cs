﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextPanel : MonoBehaviour {

    public Text text;

    void OnEnable() {
        SetText(text.text);
    }

    void OnDisable() {
        StopAllCoroutines();
    }

    public void SetText(string s) {
        StartCoroutine(DisplayText(s));
    }

    IEnumerator DisplayText(string t) {
        text.text = "";
        foreach (char c in t) {
            text.text += c.ToString();
            yield return new WaitForSeconds(0.01f);
        }
    }
}
