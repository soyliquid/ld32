﻿using UnityEngine;
using System.Collections;

public class WeaponModObject : MonoBehaviour, IPickable {

    public TextMesh text;
    public WeaponModSpec weaponMod;

	// Use this for initialization
	void Start () {
        text = GetComponentInChildren<TextMesh>();
	}

    public object PickUp() {
        Destroy(gameObject);
        return weaponMod;
    }
}
