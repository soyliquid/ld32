﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public static GameManager _instance;
    public static GameManager instance {
        get {
            return _instance;
        }
    }

    public GameObject TitlePanel;
    public Text scoreText;
    public Text playtimeText;
    public GameObject PlayPanel;
    public GameObject ResultPanel;
    public Text resultScoreText;

    public WeaponModObject modObject;
    public WeaponSpec weapon;
    public DropTable dropTable;
    public Unit player;
    public Enemy enemy;
    public Transform enelySpawnPoint;
    public bool isPlaying = true;
    public Text weaponSpecText;

    public float enemySpawnInterval = 1f;
    private float enemySpawnDelay;
    public int killCount;
    public float playTime;

    public void RefreshWeaponSpecText() {
        weaponSpecText.text = weapon.ToString();
    }

    void Awake() {
        _instance = this;
    }

	// Use this for initialization
	void Start () {
        weapon = ScriptableObject.Instantiate<WeaponSpec>(weapon);
        RefreshWeaponSpecText();
	}
	
	// Update is called once per frame
	void Update () {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            SpawnItem(player.transform.position + player.transform.forward * 2f);
        }
        if (TitlePanel.activeSelf) {
            if (Input.GetKeyDown(KeyCode.Space)) { 
                TitlePanel.SetActive(false);
                PlayPanel.SetActive(true);
                playTime = 60f;
            }
        } else if (PlayPanel.activeSelf) {
            scoreText.text = "SCORE:" + killCount;
            playtimeText.text = "TIME:" + playTime.ToString("0.00");
            if (playTime <= 0f) {
                playTime = 0f;
                resultScoreText.text = scoreText.text;
                PlayPanel.SetActive(false);
                ResultPanel.SetActive(true);
                //Time.timeScale = 0f;
            } else {
                playTime -= Time.deltaTime;
            }
            SpawnEnemy();
        } else if (ResultPanel.activeSelf) {
            if (Input.GetKeyDown(KeyCode.Space)) {
                Application.LoadLevel(Application.loadedLevel);
            }
        }
	}

    public void SpawnItem(Vector3 spawnPosition) {
        WeaponModObject o = Instantiate<WeaponModObject>(modObject);
        o.transform.position = spawnPosition;
        o.weaponMod = dropTable.RequestMod();
        o.text.text = o.weaponMod.ToString();
    }

    public void SpawnEnemy() {
        if (Time.deltaTime >= 1000f / 30f) return;
        if (enemySpawnDelay >= 0) {
            enemySpawnDelay -= Time.deltaTime;
        } else {
            enemySpawnDelay = enemySpawnInterval;
            enemySpawnInterval *= 0.995f;
            if (enemySpawnInterval < 0.005f) enemySpawnInterval = 0.005f;
            Enemy u = Instantiate<Enemy>(enemy);
            u.transform.position = enelySpawnPoint.position;
            u.transform.rotation = enelySpawnPoint.rotation;
        }
    }

}
