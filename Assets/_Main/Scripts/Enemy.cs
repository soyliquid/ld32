﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour, IDamageable {

    private Rigidbody rigidbody;
    public int HP;
    public Effect deathEffect;
    public int dropRate = 0;

    void Awake() {
        rigidbody = GetComponent<Rigidbody>();
    }

    void Start() {
        StartCoroutine(Move());
    }

	// Update is called once per frame
	void Update () {
	}

    IEnumerator Move() {
        while (isActiveAndEnabled) {
            yield return new WaitForSeconds(1f);
            rigidbody.AddForce(((GameManager.instance.player.transform.position - transform.position).normalized + transform.up) * 10f, ForceMode.VelocityChange);
        }
    }

    void IDamageable.Damage(int value) {
        HP -= value;
        if (HP <= 0) Death();
    }

    void Death() {
        Effect e = Effect.Spawn(deathEffect);
        e.transform.position = transform.position;
        e.transform.rotation = transform.rotation;
        if (UnityEngine.Random.Range(0, 100) < dropRate) {
            GameManager.instance.SpawnItem(transform.position);
        }
        GameManager.instance.killCount++;
        Destroy(gameObject);
    }
}
