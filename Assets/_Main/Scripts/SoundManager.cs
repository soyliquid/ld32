﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

    public static SoundManager instance;

    public AudioSource audio;
    public AudioClip typeSound;

    void Awake() {
        instance = this;
        audio = gameObject.AddComponent<AudioSource>();
        audio.maxDistance *= 5f;
    }
}
