﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public float moveSpeed = 1f;
    public float lookSpeed = 1f;
    public float moveSmooth = 0.5f;
    public float lookSmooth = 0.5f;

    [NonSerialized]
    public Vector3 moveAmount;
    [NonSerialized]
    public Vector3 lookAmount;
    [NonSerialized]
    public CharacterController cc;
    public AudioSource audio;
    public AudioClip pickUpSE;


    private bool isDash;

    private float fireDelay;

    void Awake() {
        cc = GetComponent<CharacterController>();
        audio = GetComponent<AudioSource>();
    }

    void Start() {
    }

	// Update is called once per frame
	void Update () {
        Fall();
        Move();
        Shot();
        CollisionCheck();
	}

    void CollisionCheck() {
        foreach (Collider c in Physics.OverlapSphere(transform.position, .4f)) {
            if(c.gameObject != gameObject) Collision(c);
        }
    }

    void Collision(Collider c) {
        IPickable pickItem = c.gameObject.GetComponent<IPickable>();
        if (pickItem == null) return;
        object o = pickItem.PickUp();
        if (typeof(WeaponModSpec) == o.GetType()) {
            audio.PlayOneShot(pickUpSE);
            WeaponModSpec mod = (WeaponModSpec)o;
            GameManager.instance.weapon.ApplyMod(mod.modType);                        
        }
    }

    void Move() {
        isDash = Input.GetKey(KeyCode.LeftShift);

        Vector3 moveInput = new Vector3(
            Input.GetAxis("Horizontal"),
            0f,
            Input.GetAxis("Vertical")
            );
        moveAmount = moveInput * moveSpeed * Time.deltaTime;
        cc.Move(transform.rotation * moveAmount);

        Vector3 lookInput = new Vector3(
            0f,
            Input.GetAxis("Mouse X"),
            0f
            );
        lookAmount = lookInput * lookSpeed * Time.deltaTime;

        transform.rotation = Quaternion.Lerp(
            transform.rotation,
            transform.rotation * Quaternion.Euler(lookAmount),
            lookSmooth);
    }

    void Fall() {
        float fallSpeed = 0.05f;
        RaycastHit hit;
        if (Physics.Raycast(transform.position - transform.up * 0.5f, -transform.up, out hit, 0.1f)) {
            transform.position = hit.point + transform.up * 0.51f;
        } else {
            transform.Translate(-transform.up * fallSpeed);
        }
    }

    void Shot() {
        if (fireDelay > 0f) {
            fireDelay -= Time.deltaTime;
        } else {
            if (Input.GetButton("Fire1")) {
                WeaponSpec weapon = GameManager.instance.weapon;
                fireDelay = weapon.fireInterval;

                audio.PlayOneShot(weapon.fireSound);

                for (int cnt = 0; cnt < weapon.projectileCount; cnt++) {
                    Projectile p = Projectile.Spawn(weapon.projectile);
                    p.transform.position = transform.position;
                    p.transform.rotation = transform.rotation;
                    p.owner = gameObject;
                    p.maxLifeTime = weapon.maxLifeTime;
                    p.damage = weapon.damage;
                    p.bounceCount = weapon.bounceCount;
                    p.rigidbody.useGravity = weapon.isFall;

                    Vector2 dp = UnityEngine.Random.insideUnitCircle * weapon.dispersion;
                    p.transform.Rotate(new Vector3(dp.x, dp.y, 1f));
                    p.rigidbody.AddForce(p.transform.rotation * weapon.launchVelocity, ForceMode.Force);

                }
            }
        }
    }
}
