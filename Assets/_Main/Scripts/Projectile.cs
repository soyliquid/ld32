﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Projectile : MonoBehaviour {

    public static Queue<Projectile> pool = new Queue<Projectile>();

    [NonSerialized]
    public Rigidbody rigidbody;
    private Vector3 prePosition;
    public int damage = 1;
    public Effect hitEffect;
    public GameObject owner;
    public int bounceCount;
    public float maxLifeTime;

    void Awake() {
        rigidbody = GetComponent<Rigidbody>();
    }

	// Update is called once per frame
	void FixedUpdate () {
        if (maxLifeTime <= 0f) Despawn();
        maxLifeTime -= Time.deltaTime;
        CheckHit();
	}

    public static Projectile Spawn(Projectile prefab) {
        if (pool.Count == 0) {
            Projectile p = Instantiate<Projectile>(prefab);
            GameObject.DontDestroyOnLoad(p);
            return p;
        } else {
            Projectile p = pool.Dequeue();
            p.gameObject.SetActive(true);
            p.rigidbody.velocity = Vector3.zero;
            p.prePosition = Vector3.zero;
            return p;
        }
    }

    void Despawn() {
        Invoke("AddPool", .1f); // prevent rapid reusing line renderer glitch.
        gameObject.SetActive(false);
    }

    void AddPool() {
        pool.Enqueue(this);
    }

    void CheckHit() {
        if (prePosition != Vector3.zero) {
            RaycastHit hit;
            Vector3 movement = transform.position - prePosition;
            if (Physics.Raycast(prePosition, movement, out hit, movement.magnitude)) {
                if (hit.collider.gameObject != owner) {
                    Effect e = Effect.Spawn(hitEffect);
                    e.transform.position = hit.point;
                    e.transform.rotation = transform.rotation;
                    IDamageable damageable = hit.collider.gameObject.GetComponent<IDamageable>();
                    if (damageable != null) {
                        damageable.Damage(damage);
                    }

                    if (bounceCount == 0) {
                        Despawn();
                    } else {
                        bounceCount--;
                        rigidbody.velocity = Vector3.Reflect(rigidbody.velocity, hit.normal);
                        //prePosition = transform.position;
                        //transform.position += rigidbody.velocity.normalized * 2f;
                        prePosition = hit.point;
                        transform.position = hit.point + rigidbody.velocity.normalized;
                        //CheckHit();
                    }
                }
            }
        }
        prePosition = transform.position;
    }
}
