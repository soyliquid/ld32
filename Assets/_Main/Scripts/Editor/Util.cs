﻿using UnityEngine;
using System.Collections;
using UnityEditor;

static class Util {
    [MenuItem("Assets/Create/ScriptableObject")]
    public static void CreateAsset() {
        ScriptableObject item = ScriptableObject.CreateInstance<ScriptableObject>();
        string path = AssetDatabase.GenerateUniqueAssetPath("Assets/ScriptableObject.asset");
        AssetDatabase.CreateAsset(item, path);
        AssetDatabase.SaveAssets();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = item;
    }
}
