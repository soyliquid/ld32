﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Effect : MonoBehaviour {

    public static Dictionary<string, Queue<Effect>> pool = new Dictionary<string, Queue<Effect>>();

    public List<ParticleSystem> particles = new List<ParticleSystem>();
    public bool oneShot;
    private string prefabName;

    public static Effect Spawn(Effect prefab) {
        if (!pool.ContainsKey(prefab.name)) {
            pool.Add(prefab.name, new Queue<Effect>());
        }
        if (pool[prefab.name].Count == 0) {
            Effect e = Instantiate<Effect>(prefab);
            e.prefabName = prefab.name;
            GameObject.DontDestroyOnLoad(e);
            return e;
        } else {
            Effect e = pool[prefab.name].Dequeue();
            e.gameObject.SetActive(true);
            return e;
        }
    }

    void Awake() {
        foreach (ParticleSystem ps in transform.GetComponentsInChildren<ParticleSystem>()) {
            particles.Add(ps);
        }
    }

    void Despawn() {
        pool[prefabName].Enqueue(this);
        gameObject.SetActive(false);
    }

	// Update is called once per frame
	void Update () {
        if (oneShot) {
            if (!particles[0].IsAlive()) {
                Despawn();
            }
        }
	}
}
