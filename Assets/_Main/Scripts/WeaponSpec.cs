﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WeaponSpec : ScriptableObject {
    public Projectile projectile;
    public AudioClip fireSound;

    public int damage = 1;
    public Vector3 launchVelocity = new Vector3(0f, 0f, 3000f);
    public float fireInterval = 0.1f;
    public float maxLifeTime = 3f;
    public float dispersion = 1f;
    public int projectileCount = 1;

    public bool isFall;
    public bool isPenetrator;
    public bool isExplosive;
    public float explosionRange;
    public bool isHoming;
    public float homingAgility;
    public int bounceCount = 0;

    public string ToString() {
        return
            "WEAPON SPEC:" + System.Environment.NewLine +
            " DMG=" + damage + System.Environment.NewLine +
            " CNT=" + projectileCount + System.Environment.NewLine +
            " VEL=" + launchVelocity.magnitude + System.Environment.NewLine +
            " RPM=" + (60f / fireInterval).ToString("0.0") + System.Environment.NewLine +
            " ACC=" + dispersion + System.Environment.NewLine +
            " BNC=" + bounceCount + System.Environment.NewLine;
            //"explosionRange=" + explosionRange + System.Environment.NewLine +
            //(isFall ? "[FALL] " : " ") + 
            //(isPenetrator ? "[PENETRATE] " : " ") + 
            //(isFall ? "[EXPLODE] " : " ") + 
            //(isHoming ? "[HOMING] " : " ");
            
    }

    public void ApplyMod(WeaponModSpecType mod) {
        switch (mod) {
            case WeaponModSpecType.DAMAGE:
                damage++;
                break;
            case WeaponModSpecType.VELOCITY:
                launchVelocity *= 1.05f;
                break;
            case WeaponModSpecType.FIRERATE:
                fireInterval *= 0.95f;
                break;
            case WeaponModSpecType.PROJECTILES:
                projectileCount++;
                break;
            case WeaponModSpecType.BOUNCE:
                bounceCount++;
                break;
            case WeaponModSpecType.ACCURACY:
                dispersion *= 0.95f;
                break;
        }
        GameManager.instance.RefreshWeaponSpecText();
    }
}
