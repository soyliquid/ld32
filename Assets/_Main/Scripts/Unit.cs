﻿using UnityEngine;
using System.Collections;
using System;

public class Unit : MonoBehaviour, IDamageable {
    public int HP;
    public Effect deathEffect;

    void IDamageable.Damage(int value) {
        HP -= value;
        if (HP <= 0) Death();
    }

    void Death() {
        Effect e = Effect.Spawn(deathEffect);
        e.transform.position = transform.position;
        e.transform.rotation = transform.rotation;
        Destroy(gameObject);
    }
}
