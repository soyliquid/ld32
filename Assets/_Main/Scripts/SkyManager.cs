﻿using UnityEngine;
using System.Collections;

public class SkyManager : MonoBehaviour {

    public Transform sky;
    public Vector3 speed;

	// Update is called once per frame
	void Update () {
        sky.transform.Rotate(speed * Time.deltaTime);
	}
}
