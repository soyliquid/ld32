﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum WeaponModSpecType { 
    DAMAGE,
    VELOCITY,
    FIRERATE,
    PROJECTILES,
    BOUNCE,
    ACCURACY
}

public class WeaponModSpec : ScriptableObject {
    public WeaponModSpecType modType;
    public int power;

    public string ToString() {
        Debug.Log(modType);
        return modType.ToString() + " UP";
    }
}
