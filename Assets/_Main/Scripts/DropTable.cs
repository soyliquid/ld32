﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class DropTable : ScriptableObject {

    public DropTableSetting[] dropTable;

    [Serializable]
    public class DropTableSetting {
        public int rarelity = 1;
        public WeaponModSpec mod;
    }

    public WeaponModSpec RequestMod() {
        int rarelity = UnityEngine.Random.Range(0, 100) + 1;
        Debug.Log("rarelity" + rarelity);
        DropTableSetting[] drops = dropTable.Where(d => d.rarelity <= rarelity).ToArray();
        Debug.Log("matchCount:" + drops.Length);
        return drops[UnityEngine.Random.Range(0, drops.Length)].mod;
    }
}
