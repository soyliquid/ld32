﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {

    public PlayerController target;
    public Vector3 lookOffset;
    public Vector3 positionOffset;
    public float moveSmooth = 0.5f;
    public float lookSmooth = 0.5f;
    public float cameraTiltAmount = 30f;

	// Update is called once per frame
	void Update () {
        if (target == null) return;
        Vector3 TP = target.transform.position + target.transform.rotation * lookOffset;
        Quaternion cameraTilt = Quaternion.Euler(0f,
            (target.moveAmount.x + target.lookAmount.y / 20f) * cameraTiltAmount,
            (target.moveAmount.x + target.lookAmount.y / 20f) * cameraTiltAmount);
        transform.rotation = Quaternion.Lerp(transform.rotation, target.transform.rotation * cameraTilt, lookSmooth);
        transform.position = Vector3.Lerp(transform.position, TP + (target.transform.rotation * positionOffset), moveSmooth);
	}
}